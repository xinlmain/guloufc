package com.gulou.calendarlib.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;

import com.gulou.calendarlib.util.CalendarViewBuilder;
import com.gulou.calendarlib.util.CalendarViewPagerLisenter;
import com.gulou.calendarlib.util.CustomDate;
import com.gulou.calendarlib.util.CustomViewPagerAdapter;
import com.gulou.calendarlib.widget.CalendarView.onCalendarChangeListener;
import com.gulou.calendarlib.widget.CalendarView.onClickDateListener;

public class SlideCalendar extends ViewPager {

	private static final String TAG = "SlideCalendar";
	private Context mContext;
	private CalendarView[] views;
	private CalendarViewBuilder builder;
	private int mCalendarStyle;
	private onChosenDateChangeListener mChosenDateChangeListener;
	private onShowDateChangeListener mShowDateChangeListener;
	private CustomDate mChosenDate;
	public CustomDate getChosenDate() {
		return mChosenDate;
	}

	private CustomDate mShowDate;

	public CustomDate getShowDate() {
		return mShowDate;
	}

	public interface onChosenDateChangeListener
	{
		void onChosenDateChange(CustomDate date);
	}
	
	public interface onShowDateChangeListener
	{
		//当calendar的ShowDate改变时
		void onShowDateChange(CustomDate date); 
	}
	
	public SlideCalendar(Context context) {
		super(context);
		mContext = context;
		mCalendarStyle = CalendarView.WEEK_STYLE;
		Init();
	}

	public SlideCalendar(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		mCalendarStyle = CalendarView.WEEK_STYLE;
		Init();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

		super.onMeasure(MeasureSpec.makeMeasureSpec(measureWidth(widthMeasureSpec),MeasureSpec.EXACTLY), 
				MeasureSpec.makeMeasureSpec(measureHeight(widthMeasureSpec, heightMeasureSpec),MeasureSpec.EXACTLY));
	}

	private void Init() {
		builder = new CalendarViewBuilder();
		views = builder.createMassCalendarViews(mContext, 3, CalendarView.WEEK_STYLE,
				new onClickDateListener() {
			
				@Override
				public void onDateClick(CustomDate date) {
//					Log.d(TAG, "date click change " 
//							+ date.year + "-" + date.month + "-" + date.day);
					if (!date.isSameDate(mChosenDate))
					{
						mChosenDate = new CustomDate(date.year, date.month, date.day);
						if (mChosenDateChangeListener != null)
						{
							mChosenDateChangeListener.onChosenDateChange(mChosenDate);
						}
					}
				}
			},
			new onCalendarChangeListener() {
				
				@Override
				public void onDateChange(CustomDate date) {
//					Log.d(TAG, "show date change " 
//							+ date.year + "-" + date.month + "-" + date.day);
					if (!date.isSameDate(mShowDate))
					{
						mShowDate = new CustomDate(date.year, date.month, date.day);
						if (mShowDateChangeListener != null)
						{
							mShowDateChangeListener.onShowDateChange(mShowDate);
						}
					}
				}
			}
			);
		CustomViewPagerAdapter<CalendarView> viewPagerAdapter = new CustomViewPagerAdapter<CalendarView>(
				views);
		this.setAdapter(viewPagerAdapter);
		this.setCurrentItem(498);
		this.setOnPageChangeListener(new CalendarViewPagerLisenter(
				viewPagerAdapter));
	}

	public void setOnChosenDateChangeListener(onChosenDateChangeListener listener) {
		mChosenDateChangeListener = listener;
	}

	public void SetOnShowDateChangeListener(onShowDateChangeListener listener) {
		mShowDateChangeListener = listener;
	}

	public void switchCalendarViewStyle(int style) {

		if (mCalendarStyle == style)
			return;
		mCalendarStyle = style;
		builder.switchCalendarViewsStyle(style);
		
		this.requestLayout();
	}

	private int measureWidth(int measureSpec) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);

		if (specMode == MeasureSpec.EXACTLY || specMode == MeasureSpec.AT_MOST) 
		{
			// Respect EXACTLY or AT_MOST mode 
			result = specSize;
		} 
		else 
		{
			Log.d(TAG, "measureWidth():  MeasureSpec = UNSPECIFIED");
		}

		return result;
	}

	private int measureHeight(int widthMeasureSpec, int measureSpec) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int width = MeasureSpec.getSize(widthMeasureSpec);
		if (specMode == MeasureSpec.EXACTLY) {
			result = width;
		} else {

			if (specMode == MeasureSpec.AT_MOST) {
				if (mCalendarStyle == CalendarView.MONTH_STYLE) {
					result = width * 6 / 7;
				} else {
					result = width / 7;
				}
			} else {
				Log.d(TAG, "measureHeight():  MeasureSpec = UNSPECIFIED");
			}
		}
		Log.d(TAG, "measureHeight():  width = " + width + " result = " + result);
		return result;
	}
}
