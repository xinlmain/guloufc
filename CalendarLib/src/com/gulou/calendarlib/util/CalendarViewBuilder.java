package com.gulou.calendarlib.util;

import android.content.Context;

import com.gulou.calendarlib.widget.CalendarView;
import com.gulou.calendarlib.widget.CalendarView.onCalendarChangeListener;
import com.gulou.calendarlib.widget.CalendarView.onClickDateListener;
/**
 * CalendarView的辅助类
 *
 */
public class CalendarViewBuilder {
		private CalendarView[] calendarViews;
		/**
		 * 生产多个CalendarView
		 * @param context
		 * @param count
		 * @param style
		 * @param callBack
		 * @return
		 */
		public  CalendarView[] createMassCalendarViews(Context context,int count,int style, onClickDateListener onclick, onCalendarChangeListener onshow){
			calendarViews = new CalendarView[count];
			for(int i = 0; i < count;i++){
				calendarViews[i] = new CalendarView(context, style, onclick, onshow);
			}
			return calendarViews;
		}
		
		public  CalendarView[] createMassCalendarViews(Context context,int count, onClickDateListener onclick, onCalendarChangeListener onshow){
			
			return createMassCalendarViews(context, count, CalendarView.WEEK_STYLE, onclick, onshow);
		}
		/**
		 * 切换CandlendarView的样式
		 * @param style
		 */
		public void switchCalendarViewsStyle(int style){
			if(calendarViews != null)
			for(int i = 0 ;i < calendarViews.length;i++){
				calendarViews[i].switchStyle(style);
			}
		}
		/**
		 * CandlendarView回到当前日期
		 */
		
		public void backTodayCalendarViews(){
			if(calendarViews != null)
			for(int i = 0 ;i < calendarViews.length;i++){
				calendarViews[i].backToday();
			}
		}
}
