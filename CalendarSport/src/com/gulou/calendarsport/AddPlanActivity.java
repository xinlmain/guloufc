package com.gulou.calendarsport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;
import android.widget.Toast;

import com.gulou.calendarlib.util.CustomDate;
import com.gulou.calendarlib.util.DateUtil;
import com.gulou.calendarlib.widget.CalendarView;
import com.gulou.calendarlib.widget.SlideCalendar;
import com.gulou.calendarsport.bll.SportType;
import com.gulou.calendarsport.util.GlobalData;
import com.gulou.calendarsport.util.HttpUtil;
import com.gulou.calendarsport.util.HttpUtil.HttpCallbackListener;
import com.gulou.calendarlib.widget.SlideCalendar.onChosenDateChangeListener;
import com.gulou.calendarlib.widget.SlideCalendar.onShowDateChangeListener;

public class AddPlanActivity extends Activity implements OnClickListener {

	private static final String TAG = "AddPlanActivity";
	private TextView mMonthTv;
	private TextView mYearTv;
	private TextView mWeekTv;
	private TextView mMonthModeTv;
	private TextView mWeekModeTv;
	private SlideCalendar mSlideCalendar;
	private RelativeLayout mSportNameRL;
	private RelativeLayout mStartTimeRL;
	private RelativeLayout mEndTimeRL;
	private TextView mConfirmTv;
	private TextView mCancelTv;
	private LinearLayout mPickerLl;
	private TextView mSportNameTv;
	private TextView mStartTimeTv;
	private TextView mEndTimeTv;
	private List<SportType> mSportTypeList;
	private List<String> mSportNameList;
	private float mLatitude;
	private float mLongitude;
	private Date mChosenDate;
	private int selectedSportIndex;
	private int startHour;
	private int startMinute;
	private int endHour;
	private int endMinute;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addplan);
		initViews();

		mSportNameList = new ArrayList<String>();

		Intent intent = this.getIntent();
		Object o = intent.getSerializableExtra("sportType");
		mSportTypeList = (List<SportType>) o;
		if (mSportTypeList != null) {
			for (int i = 0; i < mSportTypeList.size(); i++) {
				mSportNameList.add(mSportTypeList.get(i).getName());
			}
		}

		mLatitude = intent.getFloatExtra("latitude", 0);
		mLongitude = intent.getFloatExtra("longitude", 0);
	}

	private void initViews() {
		mMonthTv = (TextView) this.findViewById(R.id.show_month_tv);
		mYearTv = (TextView) this.findViewById(R.id.show_year_tv);
		mWeekTv = (TextView) this.findViewById(R.id.show_week_tv);
		mSlideCalendar = (SlideCalendar) this
				.findViewById(R.id.id_slide_calendar);
		mMonthModeTv = (TextView) this.findViewById(R.id.month_mode_tv);
		mWeekModeTv = (TextView) this.findViewById(R.id.week_mode_tv);
		mSportNameRL = (RelativeLayout) this
				.findViewById(R.id.id_rl_add_sportname);
		mStartTimeRL = (RelativeLayout) this
				.findViewById(R.id.id_rl_add_starttime);
		mEndTimeRL = (RelativeLayout) this.findViewById(R.id.id_rl_add_endtime);
		mConfirmTv = (TextView) this.findViewById(R.id.id_tv_add_confirm);
		mCancelTv = (TextView) this.findViewById(R.id.id_tv_add_cancel);
		mPickerLl = (LinearLayout) this.findViewById(R.id.id_ll_add_picker);
		mSportNameTv = (TextView) this.findViewById(R.id.id_tv_add_sportname);
		mStartTimeTv = (TextView) this.findViewById(R.id.id_tv_add_starttime);
		mEndTimeTv = (TextView) this.findViewById(R.id.id_tv_add_endtime);
		switchBackgroundForButton(false);
		mMonthModeTv.setOnClickListener(this);
		mWeekModeTv.setOnClickListener(this);
		mSportNameRL.setOnClickListener(this);
		mStartTimeRL.setOnClickListener(this);
		mEndTimeRL.setOnClickListener(this);
		mConfirmTv.setOnClickListener(this);
		mCancelTv.setOnClickListener(this);
		CustomDate showDate = mSlideCalendar.getShowDate();
		setShowDateViewText(showDate.year, showDate.month);
		mChosenDate = mSlideCalendar.getChosenDate().toDate();
		mSlideCalendar
				.SetOnShowDateChangeListener(new onShowDateChangeListener() {

					@Override
					public void onShowDateChange(CustomDate date) {
						setShowDateViewText(date.year, date.month);
					}
				});

		mSlideCalendar
				.setOnChosenDateChangeListener(new onChosenDateChangeListener() {

					@Override
					public void onChosenDateChange(CustomDate date) {
						Log.d(TAG, "date clicked: " + date.toString());
						mChosenDate = date.toDate();
						Log.d(TAG, "mChosenDate: " + mChosenDate.toString());
					}
				});
	}

	@SuppressLint("NewApi")
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.month_mode_tv:
			switchBackgroundForButton(true);
			mSlideCalendar.switchCalendarViewStyle(CalendarView.MONTH_STYLE);
			break;
		case R.id.week_mode_tv:
			switchBackgroundForButton(false);
			mSlideCalendar.switchCalendarViewStyle(CalendarView.WEEK_STYLE);
			break;
		case R.id.id_tv_add_cancel:
			// 当点击了‘取消’时, 直接返回到上一个activity.
			finish();
			break;
		case R.id.id_tv_add_confirm:
			// 当点击了‘确定’时, Post到Server端
			postFeed();
			break;

		case R.id.id_rl_add_starttime:
			if (mPickerLl.findViewWithTag("starttime") == null) {
				if (mPickerLl.getChildCount() > 0) {
					// 如果mPickerLl还有其它Picker则删除所有的Picker
					mPickerLl.removeAllViews();
				}

				TimePicker tp = new TimePicker(this);
				tp.setTag("starttime");
				tp.setOnTimeChangedListener(new OnTimeChangedListener() {

					@Override
					public void onTimeChanged(TimePicker view, int hourOfDay,
							int minute) {
						mStartTimeTv.setText(hourOfDay + ":" + minute);
						startHour = hourOfDay;
						startMinute = minute;
					}
				});
				mPickerLl.addView(tp);
			}
			break;

		case R.id.id_rl_add_endtime:
			if (mPickerLl.findViewWithTag("endtime") == null) {
				if (mPickerLl.getChildCount() > 0) {
					// 如果mPickerLl还有其它Picker则删除所有的Picker
					mPickerLl.removeAllViews();
				}

				TimePicker tp = new TimePicker(this);
				tp.setTag("endtime");
				tp.setOnTimeChangedListener(new OnTimeChangedListener() {

					@Override
					public void onTimeChanged(TimePicker view, int hourOfDay,
							int minute) {
						mEndTimeTv.setText(hourOfDay + ":" + minute);
						endHour = hourOfDay;
						endMinute = minute;
					}
				});
				mPickerLl.addView(tp);
			}
			break;

		case R.id.id_rl_add_sportname:
			if (mPickerLl.findViewWithTag("sportname") == null) {
				if (mPickerLl.getChildCount() > 0) {
					// 如果mPickerLl还有其它Picker则删除所有的Picker
					mPickerLl.removeAllViews();
				}

				NumberPicker np = new NumberPicker(this);
				final String[] city = (String[]) mSportNameList
						.toArray(new String[mSportNameList.size()]);
				np.setDisplayedValues(city);
				np.setMinValue(0);
				np.setMaxValue(city.length - 1);

				np.setTag("sportname");
				np.setOnValueChangedListener(new OnValueChangeListener() {

					@Override
					public void onValueChange(NumberPicker arg0, int arg1,
							int arg2) {
						mSportNameTv.setText(city[arg2]);
						selectedSportIndex = arg2;
					}
				});
				mPickerLl.addView(np);
			}
			break;

		default:
			break;
		}
	}

	@SuppressWarnings("deprecation")
	private void postFeed() {
		JSONObject node = new JSONObject();
		try {
			node.put("uid", GlobalData.getUid());
			node.put("memo", "new test plan");

			// get start time
			mChosenDate.setHours(startHour);
			mChosenDate.setMinutes(startMinute);

			node.put("start", mChosenDate.getTime() / 1000);

			mChosenDate.setHours(endHour);
			mChosenDate.setMinutes(endMinute);

			node.put("end", mChosenDate.getTime() / 1000);

			// sport type
			node.put("plan", mSportTypeList.get(selectedSportIndex).getId());
			JSONObject loc = new JSONObject();
			loc.put("lnt", mLatitude);
			loc.put("lat", mLongitude);
			node.put("loc", loc);

		} catch (JSONException e1) {
			//
			e1.printStackTrace();
		}
		Log.d(TAG, node.toString());
		// Toast.makeText(this, node.toString(), 20).show();

		HttpUtil.sendPostHttpRequest("http://120.24.85.99:8080/feeds",
				node.toString(), new HttpCallbackListener() {

					@Override
					public void onFinish(String response) {
						finish();
					}

					@Override
					public void onError(Exception e) {
						Log.d(TAG, e.toString());
						Toast.makeText(AddPlanActivity.this, "添加计划失败",
								Toast.LENGTH_SHORT).show();
					}
				});

	}

	// /// Helper functions /////

	private void switchBackgroundForButton(boolean isMonth) {
		if (isMonth) {
			mMonthModeTv.setBackgroundResource(R.drawable.press_left_text_bg);
			mWeekModeTv.setBackgroundColor(Color.TRANSPARENT);
		} else {
			mWeekModeTv.setBackgroundResource(R.drawable.press_right_text_bg);
			mMonthModeTv.setBackgroundColor(Color.TRANSPARENT);
		}
	}

	private void setShowDateViewText(int year, int month) {
		mYearTv.setText(year + "");
		mMonthTv.setText(month + "月");
		mWeekTv.setText(DateUtil.weekName[DateUtil.getDayOfWeek() - 1]);
	}
}
