package com.gulou.calendarsport;

import org.json.JSONException;
import org.json.JSONObject;

import com.gulou.calendarsport.util.GlobalData;
import com.gulou.calendarsport.util.HttpUtil;
import com.gulou.calendarsport.util.HttpUtil.HttpCallbackListener;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class EditProfileActivity extends Activity implements OnClickListener {

	private EditText mNameET;
	private Button mLoginBtn;
	private String openidString;
	private String qqName = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_editprofile);

		mLoginBtn = (Button) findViewById(R.id.button1);
		mNameET = (EditText) findViewById(R.id.editText1);
		mLoginBtn.setOnClickListener(this);
		
		Intent intent = this.getIntent();
		openidString = intent.getStringExtra("openidString");
		qqName = intent.getStringExtra("qqName");
		
		this.mNameET.setText(qqName);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button1:
			Login();
			break;

		default:
			break;
		}
	}

	private void Login() {
		
		JSONObject obj = new JSONObject();
		
		// code还是要带上的吧，不然一个用户重复登录，每次返回的id不就不一样了吗？
		//TODO: code是 openidString还是access_token？
		try {
			obj.put("nick", EditProfileActivity.this.mNameET.getText().toString());
			obj.put("code", openidString);
			obj.put("type", 1);
		} catch (JSONException e1) {
			// TODO 自动生成的 catch 块
			e1.printStackTrace();
		}
		
		HttpUtil.sendPostHttpRequest("http://120.24.85.99:8080/user", obj.toString(), new HttpCallbackListener() {
			
			@Override
			public void onFinish(String response) {
				String uid = response;
				// TODO: 未测试
				// 将uid保存，程序启动时查UID，如果找到则认为已经登录了。
				SharedPreferences sPrefs = getSharedPreferences(
						"userSetting", 0);
				sPrefs.edit().putInt("UID", Integer.parseInt(uid))
						.commit();
				// 此时应设置登录状态
				GlobalData.setUid(Integer.parseInt(uid));
				EditProfileActivity.this.finish();
			}
			
			@Override
			public void onError(Exception e) {
				// TODO 自动生成的方法存根
				
			}
		});
	}
}
