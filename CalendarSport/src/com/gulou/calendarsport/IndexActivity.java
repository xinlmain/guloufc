package com.gulou.calendarsport;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class IndexActivity extends Activity{
	
	private final String TAG = "IndexActivity";
	private LocationManager locationManager;
	public Location mLocation;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_index);
		// test on virtual device, cannot get network location
		getTestLocationAndProceed();
		//getLocation();
	}

	private void getTestLocationAndProceed() {
		mLocation = new Location(LocationManager.GPS_PROVIDER);
		mLocation.setLatitude(39.963175);
		mLocation.setLongitude(116.400244);
		Intent intent = new Intent();
		intent.setClass(this, MainActivity.class);
		intent.putExtra("latitude", mLocation.getLatitude());
		intent.putExtra("longitude", mLocation.getLongitude());
		startActivity(intent);
		finish();
	}
	
	private void getLocation() {
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		String provider;
		List<String> providerList = locationManager.getProviders(true);
		
		if (providerList.contains(LocationManager.GPS_PROVIDER))
		{
			Log.d(TAG, "GPS is enabled");
			provider = LocationManager.GPS_PROVIDER;
		}
		else
		if (providerList.contains(LocationManager.NETWORK_PROVIDER)) 
		{
			provider = LocationManager.NETWORK_PROVIDER;
		} 
		else 
		{
			Toast.makeText(this, "no location provider", Toast.LENGTH_SHORT)
					.show();
			return;
		}
		
		Location location = null;
		
		// 首先使用network获取位置, 因为比较快
		if (providerList.contains(LocationManager.NETWORK_PROVIDER)) 
		{
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,
					locationListener);
		
			//location = 
					locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//			if(location != null)
//			{
//				Log.d(TAG, "location get from network provider: " + location.getLatitude() + 
//						", " + location.getLongitude());
//				locationManager.removeUpdates(locationListener);
//			}
		}
		else
		{
			// 此时使用GPS
			location = locationManager.getLastKnownLocation(provider);
			locationManager.requestLocationUpdates(provider, 5000, 0,
					locationListener);
		}

//		if (location != null) {
//			mLocation = location;
//			
//			getTestLocationAndProceed();
//		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (locationManager != null) {
			locationManager.removeUpdates(locationListener);
		}
	}

	LocationListener locationListener = new LocationListener() {

		@Override
		public void onLocationChanged(Location location) {
			Log.d(TAG, "location changed: " + location.getLatitude() + 
					", " + location.getLongitude());
			if (mLocation == null)
			{
				Intent intent = new Intent();
				intent.setClass(IndexActivity.this, MainActivity.class);
				intent.putExtra("latitude", location.getLatitude());
				intent.putExtra("longitude", location.getLongitude());
				startActivity(intent);
			}
			mLocation = location;
			finish();
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};
}
