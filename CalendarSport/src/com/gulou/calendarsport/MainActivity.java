package com.gulou.calendarsport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.SDKInitializer;
import com.gulou.calendarlib.util.DateUtil;
import com.gulou.calendarlib.widget.CalendarView;
import com.gulou.calendarlib.widget.SlideCalendar;
import com.gulou.calendarsport.bll.SportPlan;
import com.gulou.calendarsport.bll.SportType;
import com.gulou.calendarsport.util.GlobalData;
import com.gulou.calendarsport.util.HttpUtil;
import com.gulou.calendarsport.util.HttpUtil.HttpCallbackListener;

public class MainActivity extends FragmentActivity implements OnClickListener {

	protected static final String TAG = "MainActivity";	
	private static final int INIT_VIEW = 1;
	private static final int SERVER_ERROR = 2;
	private TextView mMonthTv;
	private TextView mYearTv;
	private TextView mWeekTv;
	private TextView mMonthModeTv;
	private TextView mWeekModeTv;
	private TextView mDiscoverTv;
	private TextView mMyselfTv;
	private ImageView mTabline;
	private LinearLayout mDiscoverLl;
	private LinearLayout mAddLl;
	private LinearLayout mMyselfLl;
	private SlideCalendar mSlideCalendar;
	private FragmentManager fragmentManager;
	private DiscoverFragment mDiscoverFragment;
	private MyselfFragment mMyselfFragment;
	private PlanDetailFragment mPlanDetailFragment;
	private EditPlanFragment mEditPlanFragment;
	private List<SportType> mSportTypeList;
	public SportPlan mSelectedPlan;
	public SportPlan mEditPlan;
	private LocationManager locationManager;
	public Location mLocation;
	private int mCurrentFragmentIndex;
	public int mCurrentUid;
	
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg){
			switch (msg.what){
			case INIT_VIEW:
				initViews();
				break;
			case SERVER_ERROR:
				Toast.makeText(MainActivity.this, "网络有问题，未连接到服务器，请重启", Toast.LENGTH_SHORT).show();
				finish();
				break;
			case 3:
				Toast.makeText(MainActivity.this, 
						"GPS位置更新" + mLocation.getLatitude() + ", " + mLocation.getLongitude(), 
						Toast.LENGTH_SHORT).show();
			}
			
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// baidu map initialization
		SDKInitializer.initialize(getApplicationContext());
		
		// get location from intent
		mLocation = new Location(LocationManager.NETWORK_PROVIDER);
		mLocation.setLatitude(getIntent().getDoubleExtra("latitude", 0));
		mLocation.setLongitude(getIntent().getDoubleExtra("longitude", 0));
		
		getSportTypes();
		//initViews();
		getStoredUid();
		getGPSLocation();
	}

	// 由index传过来的location是通过网络得到的，不够精准，但是能用
	// 在main activity中再使用耗时的GPS得到精确的location
	private void getGPSLocation() {
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		String provider;
		List<String> providerList = locationManager.getProviders(true);
		if (providerList.contains(LocationManager.GPS_PROVIDER))
		{
			Log.d(TAG, "GPS is enabled");
			provider = LocationManager.GPS_PROVIDER;
		}
		else 
		{
			Toast.makeText(this, "GPS功能没有被启用", Toast.LENGTH_SHORT)
					.show();
			return;
		}

		locationManager.requestLocationUpdates(provider, 1000, 10, locationListener);
		locationManager.getLastKnownLocation(provider);
	}

	private void initViews() {
		setContentView(R.layout.activity_main);
		
		mMonthTv = (TextView) this.findViewById(R.id.show_month_tv);
		mYearTv = (TextView) this.findViewById(R.id.show_year_tv);
		mWeekTv = (TextView) this.findViewById(R.id.show_week_tv);
		mMonthModeTv = (TextView) this.findViewById(R.id.month_mode_tv);
		mWeekModeTv = (TextView) this.findViewById(R.id.week_mode_tv);
		mDiscoverTv = (TextView) this.findViewById(R.id.id_tv_discover);
		mMyselfTv = (TextView) this.findViewById(R.id.id_tv_myself);
		mTabline = (ImageView) this.findViewById(R.id.id_iv_tabline);
		mDiscoverLl = (LinearLayout) this.findViewById(R.id.id_ll_discover);
		mAddLl = (LinearLayout) this.findViewById(R.id.id_ll_add);
		mMyselfLl = (LinearLayout) this.findViewById(R.id.id_ll_myself);
		mMonthModeTv.setOnClickListener(this);
		mWeekModeTv.setOnClickListener(this);
		mDiscoverLl.setOnClickListener(this);
		mAddLl.setOnClickListener(this);
		mMyselfLl.setOnClickListener(this);
		initTabLineWidth();
		switchBackgroundForButton(false);
		fragmentManager = getSupportFragmentManager();
		// 第一次启动时选中第0个tab
		setTabSelection(0);
	}

	// 实现OnClickListener接口

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.month_mode_tv:
			switchBackgroundForButton(true);
			if (mSlideCalendar != null) {
				mSlideCalendar.switchCalendarViewStyle(CalendarView.MONTH_STYLE);
			}
			break;
		case R.id.week_mode_tv:
			switchBackgroundForButton(false);
			if (mSlideCalendar != null) {
				mSlideCalendar.switchCalendarViewStyle(CalendarView.WEEK_STYLE);
			}
			break;
		case R.id.id_ll_discover:
			// 当点击了发现tab时，选中第1个tab
			setTabSelection(0);
			break;
		case R.id.id_ll_myself:
			// 当点击了我的tab时，选中第2个tab
			
			// 如果没有登录，先登录
			if (GlobalData.getUid() == -1)
			{
				Intent intent2 = new Intent();
				intent2.setClass(MainActivity.this, LoginActivity.class);
				startActivity(intent2);
			}
			
			setTabSelection(1);
			break;
		case R.id.id_ll_add:
			// 如果没有登录，先登录
			if (GlobalData.getUid() == -1)
			{
				Intent intent2 = new Intent();
				intent2.setClass(MainActivity.this, LoginActivity.class);
				startActivity(intent2);
			}
			else
			{
				Intent intent = new Intent();
				intent.setClass(MainActivity.this, AddPlanActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable("sportType", (Serializable) mSportTypeList);
				intent.putExtras(bundle);
				
				if (mLocation != null)
				{
					intent.putExtra("latitude", mLocation.getLatitude());
					intent.putExtra("longitude", mLocation.getLongitude());
				}
		
				startActivity(intent);
			}

		default:
			break;
		}
	}

	private void setTabSelection(int index) {
		// 每次选中之前先清楚掉上次的选中状态
		clearSelection();
		// 开启一个Fragment事务
		android.support.v4.app.FragmentTransaction transaction = fragmentManager
				.beginTransaction();
		// 先隐藏掉所有的Fragment，以防止有多个Fragment显示在界面上的情况
		hideFragments(transaction);
		switch (index) {
		case 0:
			// 当点击了消息tab时，改变控件的图片和文字颜色
			mDiscoverTv.setTextColor(Color.parseColor("#008000"));
			mMyselfTv.setTextColor(Color.BLACK);
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mTabline.getLayoutParams();
			lp.leftMargin = 0;
			mTabline.setLayoutParams(lp);
			if (mDiscoverFragment == null) {
				// 如果DiscoverFragment为空，则创建一个并添加到界面上
				mDiscoverFragment = new DiscoverFragment();
				transaction.add(R.id.id_frame_content, mDiscoverFragment);
			} else {
				// 如果DiscoverFragment不为空，则直接将它显示出来
				transaction.show(mDiscoverFragment);
			}
			break;
		case 1:
			mDiscoverTv.setTextColor(Color.BLACK);
			mMyselfTv.setTextColor(Color.parseColor("#008000"));
			LinearLayout.LayoutParams lp2 = (LinearLayout.LayoutParams) mTabline.getLayoutParams();
			lp2.leftMargin = lp2.width * 2;
			mTabline.setLayoutParams(lp2);
			if (mMyselfFragment == null) {
				mMyselfFragment = new MyselfFragment();
				transaction.add(R.id.id_frame_content, mMyselfFragment);
			} else {
				transaction.show(mMyselfFragment);
			}
			break;

		case 2:
			if (mPlanDetailFragment == null) {
				mPlanDetailFragment = new PlanDetailFragment();
				transaction.add(R.id.id_frame_content, mPlanDetailFragment);
			} else {
				transaction.show(mPlanDetailFragment);
			}
			break;
			
		case 3:
			if (mEditPlanFragment == null) {
				mEditPlanFragment = new EditPlanFragment();
				transaction.add(R.id.id_frame_content, mEditPlanFragment);
			} else {
				transaction.show(mEditPlanFragment);
			}
			break;

		default:
			break;
		}
		transaction.commit();
		mCurrentFragmentIndex = index;
	}

	private void hideFragments(
			android.support.v4.app.FragmentTransaction transaction) {
		if (mDiscoverFragment != null) {
			transaction.hide(mDiscoverFragment);
		}
		if (mMyselfFragment != null) {
			transaction.hide(mMyselfFragment);
		}
		if (mPlanDetailFragment != null) {
			transaction.hide(mPlanDetailFragment);
		}
		if (mEditPlanFragment != null) {
			transaction.hide(mEditPlanFragment);
		}
	}

	private void clearSelection() {
		// TODO 自动生成的方法存根

	}

	// 用于在DiscoverFragment中响应list的click事件, 跳转到PlanDetailFragment.
	public void ShowPlanDetailFragment(SportPlan sp) {
		this.mSelectedPlan = sp;
		setTabSelection(2);
	}
	
	// 用于在MyselfFragment中响应list的click事件, 跳转到EditPlanFragment.
	public void ShowEditPlanFragment(SportPlan sp) {
		this.mEditPlan = sp;
		setTabSelection(3);
	}	

	public void setSlideCalendarForMainActivity(SlideCalendar sc) {
		this.mSlideCalendar = sc;
	}

	public void getSportTypes() {
		HttpUtil.sendGetHttpRequest("http://120.24.85.99:8080/activity",
				new HttpCallbackListener() {

					@Override
					public void onFinish(String response) {
						String line = response;
						Log.d(TAG, line);
						JSONArray arr;
						try {
							arr = new JSONArray(line);
							mSportTypeList = new ArrayList<SportType>();
							for (int i = 0; i < arr.length(); i++) {
								JSONObject temp = (JSONObject) arr.get(i);
								int id = temp.getInt("type");
								String name = temp.getString("memo");
								Log.d("heihei", id + " : " + name);

								SportType st = new SportType(id, name);

								mSportTypeList.add(st);
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
						
						// 从服务器得到了sport types 之后就可以显示界面了
						Message message = new Message();
						message.what = INIT_VIEW;
						handler.sendMessage(message);
					}

					@Override
					public void onError(Exception e) {
						Message message = new Message();
						message.what = SERVER_ERROR;
						handler.sendMessage(message);
					}
				});
	}

	// /// Helper functions /////

	private void switchBackgroundForButton(boolean isMonth) {
		if (isMonth) {
			mMonthModeTv.setBackgroundResource(R.drawable.press_left_text_bg);
			mWeekModeTv.setBackgroundColor(Color.TRANSPARENT);
		} else {
			mWeekModeTv.setBackgroundResource(R.drawable.press_right_text_bg);
			mMonthModeTv.setBackgroundColor(Color.TRANSPARENT);
		}
	}

	public void setShowDateViewText(int year, int month) {
		mYearTv.setText(year + "");
		mMonthTv.setText(month + "月");
		mWeekTv.setText(DateUtil.weekName[DateUtil.getDayOfWeek() - 1]);
	}
	
	LocationListener locationListener = new LocationListener() {

		@Override
		public void onLocationChanged(Location location) {
			Log.d(TAG, "location changed: " + location.getLatitude() + 
					", " + location.getLongitude());
			mLocation = location;
			locationManager.removeUpdates(locationListener);
			Message message = new Message();
			message.what = 3;
			handler.sendMessage(message);
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};
	
	private void getStoredUid() {
		SharedPreferences sPrefs = getSharedPreferences(
				"userSetting", 0);
		GlobalData.setUid(sPrefs.getInt("UID", -1));
		Toast.makeText(this, "stored uid:" + GlobalData.getUid(), Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// 当处于plan detail和edit plan fragment时，按返回键应该返回上一个fragment.
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			if (mCurrentFragmentIndex == 2) {
				setTabSelection(0);
				return true;
			}
			if (mCurrentFragmentIndex == 3) {
				setTabSelection(1);
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private void initTabLineWidth() {
        DisplayMetrics dpMetrics = new DisplayMetrics();
        getWindow().getWindowManager().getDefaultDisplay()
                .getMetrics(dpMetrics);
        int screenWidth = dpMetrics.widthPixels;
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mTabline
                .getLayoutParams();
        lp.width = screenWidth / 3;
        mTabline.setLayoutParams(lp);
    }

}
