package com.gulou.calendarsport.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class HttpUtil 
{
	public interface HttpCallbackListener
	{
		void onFinish(String response);
		void onError(Exception e);
	}
	
	public static void sendGetHttpRequest(final String address, 
			final HttpCallbackListener listener)
	{
		new Thread(new Runnable(){
			@Override
			public void run()
			{
				HttpURLConnection connection = null;
				try 
				{
					URL url = new URL(address);
					connection = (HttpURLConnection) url.openConnection();
					connection.setRequestMethod("GET");// 提交模式
			        connection.setConnectTimeout(8000);//连接超时 单位毫秒
			        connection.setReadTimeout(8000);//读取超时 单位毫秒
			        InputStream in = connection.getInputStream();

			        String response = convertStreamToString(in);
			        if (listener != null)
			        {
			        	listener.onFinish(response);
			        }
				}
				catch (Exception e)
				{
					if (listener != null)
					{
						listener.onError(e);
					}
				}
				finally
				{
					if (connection != null)
					{
						connection.disconnect();
					}
				}
			}
		}).start();
	}
	
	public static void sendPostHttpRequest(final String address, final String params, 
			final HttpCallbackListener listener)
	{
		new Thread(new Runnable(){
			@Override
			public void run()
			{
				HttpURLConnection connection = null;
				try 
				{
					URL url = new URL(address);
					connection = (HttpURLConnection) url.openConnection();
					connection.setRequestMethod("POST");// 提交模式
			        connection.setConnectTimeout(8000);//连接超时 单位毫秒
			        connection.setReadTimeout(8000);//读取超时 单位毫秒
			        connection.setDoOutput(true);// 是否输入参数
			        connection.setRequestProperty("Accept", "application/json"); // 设置接收数据的格式  
			        connection.setRequestProperty("Content-Type", "application/json"); // 设置发送数据的格式  

			        
			        byte[] bypes = params.toString().getBytes();
			        connection.getOutputStream().write(bypes);// 输入参数
			        InputStream in = connection.getInputStream();

			        String response = convertStreamToString(in);
			        if (listener != null)
			        {
			        	listener.onFinish(response);
			        }
				}
				catch (Exception e)
				{
					if (listener != null)
					{
						listener.onError(e);
					}
				}
				finally
				{
					if (connection != null)
					{
						connection.disconnect();
					}
				}
			}
		}).start();
	}
	
	public static String convertStreamToString(InputStream is) {      
        StringBuilder sb1 = new StringBuilder();      
        byte[] bytes = new byte[100];    
        int size = 0;    
          
        try {      
            while ((size = is.read(bytes)) > 0) {    
                String str = new String(bytes, 0, size, "UTF-8");    
                sb1.append(str);    
            }    
        } catch (IOException e) {      
            e.printStackTrace();      
        } finally {      
            try {      
                is.close();      
            } catch (IOException e) {      
               e.printStackTrace();      
            }      
        }      
        return sb1.toString();      
    }
}
