package com.gulou.calendarsport;

import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gulou.calendarsport.bll.SportPlan;

public class PlanListAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<SportPlan> mPlanList;
	private java.text.DateFormat mTimeFormat = new java.text.SimpleDateFormat("hh:mm", Locale.US);

	
	public PlanListAdapter(Context context, ArrayList<SportPlan> planList) {
		this.context = context;
		this.mPlanList = planList;
	}

	@Override
	public int getCount() {
		return mPlanList.size();
	}

	@Override
	public Object getItem(int position) {
		return mPlanList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		SportPlan plan = mPlanList.get(position);
		
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.planlistitem, parent, false);
		}
		
		TextView ownerNameTv = (TextView) convertView.findViewById(R.id.id_owernname_tv);
		ownerNameTv.setText(plan.getOwner().getName());
		
		TextView sportNameTv = (TextView) convertView.findViewById(R.id.id_sportname_tv);
		sportNameTv.setText(plan.getSportType().getName());
		
		TextView startTimeTv = (TextView) convertView.findViewById(R.id.id_starttime_tv);
		startTimeTv.setText(mTimeFormat.format(plan.getStartDate()));
		
		TextView endTimeTv = (TextView) convertView.findViewById(R.id.id_endtime_tv);
		endTimeTv.setText(mTimeFormat.format(plan.getEndDate()));
		
		TextView distanceTv = (TextView) convertView.findViewById(R.id.id_distance_tv);
		distanceTv.setText(plan.getDistance() + " km");
		
		TextView addressTv = (TextView) convertView.findViewById(R.id.id_tv_planaddress);
		addressTv.setText(plan.getAddress());
		
		return convertView;
	}
}
