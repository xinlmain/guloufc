package com.gulou.calendarsport;

import org.json.JSONException;
import org.json.JSONObject;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.InfoWindow.OnInfoWindowClickListener;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.overlayutil.PoiOverlay;
import com.baidu.mapapi.search.core.CityInfo;
import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiCitySearchOption;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiDetailSearchOption;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;
import com.baidu.mapapi.search.sug.OnGetSuggestionResultListener;
import com.baidu.mapapi.search.sug.SuggestionResult;
import com.baidu.mapapi.search.sug.SuggestionSearch;
import com.baidu.mapapi.search.sug.SuggestionSearchOption;
import com.gulou.calendarlib.util.DateUtil;
import com.gulou.calendarsport.bll.SportPlan;
import com.gulou.calendarsport.bll.User;
import com.gulou.calendarsport.util.GlobalData;
import com.gulou.calendarsport.util.HttpUtil;
import com.gulou.calendarsport.util.HttpUtil.HttpCallbackListener;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class EditPlanFragment extends Fragment 
				implements OnClickListener, OnGetSuggestionResultListener, OnGetPoiSearchResultListener {

	protected static final String TAG = "EditPlanFragment";
	private SportPlan mSportPlan;
	private TextView mSportType_Tv;
	private TextView mPlanTime_Tv;
	private TextView mMemo_Et;
	private TextView mAddress_Tv;
	private TextView mHeadCount_Tv;
	private TextView mOwnerName_Tv;
	private LinearLayout mfollowerPhotos_LL;
	private LinearLayout mfollowerNames_LL;
	private LinearLayout[] mfollowerPhotoLayouts;
	private LinearLayout[] mfollowerNameLayouts;
	private MapView mapView;
	private BaiduMap baiduMap;
	private boolean mIsEditable;
	private PoiSearch mPoiSearch;
	private SuggestionSearch mSuggestionSearch = null;
	private ArrayAdapter<String> sugAdapter = null;
	private EditText mCityET;
	private AutoCompleteTextView mkeyWorldsView;
	private Button mSearchBtn;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View pdLayout = inflater.inflate(R.layout.fragment_editplan, container,
				false);

		mSportType_Tv = (TextView) pdLayout.findViewById(R.id.id_sportname_tv);
		mPlanTime_Tv = (TextView) pdLayout.findViewById(R.id.id_plantime_tv);
		mMemo_Et = (TextView) pdLayout.findViewById(R.id.ep_memo_et);
		mAddress_Tv = (TextView) pdLayout.findViewById(R.id.id_address_tv);
		mHeadCount_Tv = (TextView) pdLayout.findViewById(R.id.id_headcount_tv);
		mOwnerName_Tv = (TextView) pdLayout.findViewById(R.id.id_planowername);
		mfollowerPhotos_LL = (LinearLayout) pdLayout
				.findViewById(R.id.id_followersphoto_ll);
		mfollowerNames_LL = (LinearLayout) pdLayout
				.findViewById(R.id.id_followernames_ll);

		mfollowerPhotoLayouts = new LinearLayout[5];
		mfollowerPhotoLayouts[0] = (LinearLayout) pdLayout
				.findViewById(R.id.id_followerphoto1);
		mfollowerPhotoLayouts[1] = (LinearLayout) pdLayout
				.findViewById(R.id.id_followerphoto2);
		mfollowerPhotoLayouts[2] = (LinearLayout) pdLayout
				.findViewById(R.id.id_followerphoto3);
		mfollowerPhotoLayouts[3] = (LinearLayout) pdLayout
				.findViewById(R.id.id_followerphoto4);
		mfollowerPhotoLayouts[4] = (LinearLayout) pdLayout
				.findViewById(R.id.id_followerphoto5);
		mfollowerNameLayouts = new LinearLayout[5];
		mfollowerNameLayouts[0] = (LinearLayout) pdLayout
				.findViewById(R.id.id_followername1);
		mfollowerNameLayouts[1] = (LinearLayout) pdLayout
				.findViewById(R.id.id_followername2);
		mfollowerNameLayouts[2] = (LinearLayout) pdLayout
				.findViewById(R.id.id_followername3);
		mfollowerNameLayouts[3] = (LinearLayout) pdLayout
				.findViewById(R.id.id_followername4);
		mfollowerNameLayouts[4] = (LinearLayout) pdLayout
				.findViewById(R.id.id_followername5);
		mapView = (MapView) pdLayout.findViewById(R.id.map_View);
		mCityET = (EditText) pdLayout.findViewById(R.id.city_ET);
		mSearchBtn = (Button) pdLayout.findViewById(R.id.search_btn);
		baiduMap = mapView.getMap();
		baiduMap.setMyLocationEnabled(true);

		this.mMemo_Et.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				/*
				 * When focus is lost check that the text field has valid
				 * values.
				 */
				if (!hasFocus) {
					Toast.makeText(getActivity(), "text changed",
							Toast.LENGTH_SHORT).show();
					postPlanUpdate();
				}
			}

		});
		updateViews();
		mkeyWorldsView = (AutoCompleteTextView) pdLayout.findViewById(R.id.searchkey_ET);
		sugAdapter = new ArrayAdapter<String>(this.getActivity(),
				android.R.layout.simple_dropdown_item_1line);
		mkeyWorldsView.setAdapter(sugAdapter);
		mSuggestionSearch = SuggestionSearch.newInstance();
		mSuggestionSearch.setOnGetSuggestionResultListener(this);
		mkeyWorldsView.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable arg0) {

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				if (cs.length() <= 0) {
					return;
				}
				String city = mCityET.getText().toString();
				/**
				 * 使用建议搜索服务获取建议列表，结果在onSuggestionResult()中更新
				 */
				mSuggestionSearch
						.requestSuggestion((new SuggestionSearchOption())
								.keyword(cs.toString()).city(city));
			}
		});
		
		mPoiSearch = PoiSearch.newInstance();

		mPoiSearch.setOnGetPoiSearchResultListener(this);

		mSearchBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mPoiSearch.searchInCity((new PoiCitySearchOption())  
					    .city(mCityET.getText().toString())  
					    .keyword(mkeyWorldsView.getText().toString()));
			}
		});
		
		return pdLayout;
	}
	
	@Override
	public void onPause() {
		super.onPause();
		mapView.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		mapView.onResume();
	}

	@Override
	public void onDestroy() {
		mPoiSearch.destroy();
		mSuggestionSearch.destroy();
		mapView.onDestroy();
		super.onDestroy();
	}

	private void postPlanUpdate() {
		// 如果memo被编辑了就更新server
		if (mSportPlan.getMemo() != mMemo_Et.getText()) {
			JSONObject node = new JSONObject();
			try {
				node.put("memo", mMemo_Et.getText());

			} catch (JSONException e1) {
				//
				e1.printStackTrace();
			}
			HttpUtil.sendPostHttpRequest(
					"http://120.24.85.99:8080/feeds/559bd39f0cf292c4d38c417e",
					node.toString(), new HttpCallbackListener() {

						@Override
						public void onFinish(String response) {
							// TODO 自动生成的方法存根

						}

						@Override
						public void onError(Exception e) {
							// Toast.makeText(getActivity(), "更改plan失败",
							// Toast.LENGTH_SHORT).show();
							Log.d(TAG, e.toString());
						}
					});
		}
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			updateViews();
		}
	}

	private void updateViews() {
		mSportPlan = ((MainActivity) this.getActivity()).mEditPlan;
		// 只有 plan是自己创建的，才可以更改。
		mIsEditable = mSportPlan.getOwner().getId() == GlobalData.getUid();

		mSportType_Tv.setText(mSportPlan.getSportType().getName());
		mPlanTime_Tv.setText(DateUtil.toHourMinute(mSportPlan.getStartDate())
				+ "-" + DateUtil.toHourMinute(mSportPlan.getEndDate()));
		mHeadCount_Tv.setText(mSportPlan.getFollowers().length + "人");
		String memo = mSportPlan.getMemo();
		mMemo_Et.setText((memo == "" || memo == null) ? "无备注" : memo);
		mAddress_Tv.setText(mSportPlan.getAddress());
		mOwnerName_Tv.setText(mSportPlan.getOwner().getName());

		User[] followers = mSportPlan.getFollowers();
		for (int i = 0; i < followers.length; i++) {
			ImageView photo = new ImageView((MainActivity) this.getActivity());
			photo.setImageResource(R.drawable.headportrait);
			LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT);

			photo.setLayoutParams(lp);

			if (i < 5) {
				mfollowerPhotoLayouts[i].addView(photo);
			}

			TextView name = new TextView((MainActivity) this.getActivity());
			name.setText(followers[i].getName());
			name.setTextSize(11);
			name.setLayoutParams(lp);
			name.setGravity(Gravity.CENTER);
			if (i < 5) {
				mfollowerNameLayouts[i].addView(name);
			}
		}

		// 设置我的位置
		Location myLocation = ((MainActivity) this.getActivity()).mLocation;
		MyLocationData.Builder locationBuilder = new MyLocationData.Builder();
		locationBuilder.latitude(myLocation.getLatitude());
		locationBuilder.longitude(myLocation.getLongitude());
		MyLocationData locationData = locationBuilder.build();
		baiduMap.setMyLocationData(locationData);

		Location location = mSportPlan.getLocation();
		if (location.getLatitude() == 0 && location.getAltitude() == 0) {
			mapView.setVisibility(View.GONE);
			mAddress_Tv.setText("未指定地址");
		} else {
			mapView.setVisibility(View.VISIBLE);
			mAddress_Tv.setText(mSportPlan.getAddress());
		}
		navigateTo(mSportPlan.getLocation());
	}

	private void navigateTo(Location location) {

		LatLng point = new LatLng(location.getLatitude(),
				location.getLongitude());
		MapStatusUpdate update = MapStatusUpdateFactory.newLatLng(point);
		baiduMap.animateMapStatus(update);
		update = MapStatusUpdateFactory.zoomTo(16f);
		baiduMap.animateMapStatus(update);

		// 构建Marker图标
		BitmapDescriptor bitmap = BitmapDescriptorFactory
				.fromResource(R.drawable.icon_gcoding);
		// 构建MarkerOption，用于在地图上添加Marker
		OverlayOptions option = new MarkerOptions().position(point)
				.icon(bitmap);
		// 在地图上添加Marker，并显示
		baiduMap.addOverlay(option);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ep_memo_et:
			break;
		default:
			break;
		}
	}

	@Override
	public void onGetSuggestionResult(SuggestionResult res) {
		if (res == null || res.getAllSuggestions() == null) {
			return;
		}
		sugAdapter.clear();
		for (SuggestionResult.SuggestionInfo info : res.getAllSuggestions()) {
			if (info.key != null)
				sugAdapter.add(info.key);
		}
		sugAdapter.notifyDataSetChanged();
	}
	
	public void onGetPoiResult(PoiResult result){  

		if (result == null
				|| result.error == SearchResult.ERRORNO.RESULT_NOT_FOUND) {
			Toast.makeText(getActivity(), "未找到结果", Toast.LENGTH_LONG)
			.show();
			return;
		}
		if (result.error == SearchResult.ERRORNO.NO_ERROR) {
			baiduMap.clear();
			PoiOverlay overlay = new MyPoiOverlay(baiduMap);
			baiduMap.setOnMarkerClickListener(overlay);
			overlay.setData(result);
			overlay.addToMap();
			overlay.zoomToSpan();
			return;
		}
		if (result.error == SearchResult.ERRORNO.AMBIGUOUS_KEYWORD) {

			// 当输入关键字在本市没有找到，但在其他城市找到时，返回包含该关键字信息的城市列表
			String strInfo = "在";
			for (CityInfo cityInfo : result.getSuggestCityList()) {
				strInfo += cityInfo.city;
				strInfo += ",";
			}
			strInfo += "找到结果";
			Toast.makeText(getActivity(), strInfo, Toast.LENGTH_LONG)
					.show();
		}
    }  
    public void onGetPoiDetailResult(PoiDetailResult result){  
    //获取Place详情页检索结果  
    } 
    
    private class MyPoiOverlay extends PoiOverlay {

		public MyPoiOverlay(BaiduMap baiduMap) {
			super(baiduMap);
		}

		@Override
		public boolean onPoiClick(int index) {
			super.onPoiClick(index);
			final PoiInfo poi = getPoiResult().getAllPoi().get(index);
			//创建InfoWindow展示的view  
			Button button = new Button(getActivity().getApplicationContext());
			button.setBackgroundResource(R.drawable.popup);
			button.setText("设为地址");
			button.setTextColor(android.graphics.Color.BLUE);
			// 定义用于显示该InfoWindow的坐标点
			LatLng pt = new LatLng(poi.location.latitude,
					poi.location.longitude);
			OnInfoWindowClickListener listener = new OnInfoWindowClickListener() {
				public void onInfoWindowClick() {
					Toast.makeText(getActivity(), 
							"设置plan地址" + poi.location.latitude + poi.location.longitude, 
							Toast.LENGTH_LONG).show();
				}
			};
			// 创建InfoWindow , 传入 view， 地理坐标， y 轴偏移量
			InfoWindow mInfoWindow = new InfoWindow(BitmapDescriptorFactory.fromView(button), pt, -47, listener);
			// 显示InfoWindow
			baiduMap.showInfoWindow(mInfoWindow);
			return true;
		}
	}
}