package com.gulou.calendarsport;

import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.gulou.calendarlib.util.DateUtil;
import com.gulou.calendarsport.bll.SportPlan;
import com.gulou.calendarsport.bll.User;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PlanDetailFragment extends Fragment {
	private SportPlan mSportPlan;
	private TextView mSportType_Tv;
	private TextView mPlanTime_Tv;
	private TextView mMemo_Tv;
	private TextView mAddress_Tv;
	private TextView mHeadCount_Tv;
	private TextView mOwnerName_Tv;
	private ImageView mOwnerPhoto_Iv;
	private LinearLayout mfollowerPhotos_LL;
	private LinearLayout mfollowerNames_LL;
	private LinearLayout[] mfollowerPhotoLayouts;
	private LinearLayout[] mfollowerNameLayouts;
	private MapView mapView;
	private BaiduMap baiduMap;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View pdLayout = inflater.inflate(R.layout.fragment_plandetail,
				container, false);
		
		mSportType_Tv = (TextView)pdLayout.findViewById(R.id.id_sportname_tv);
		mPlanTime_Tv = (TextView)pdLayout.findViewById(R.id.id_plantime_tv);
		mMemo_Tv = (TextView)pdLayout.findViewById(R.id.id_memo_tv);
		mAddress_Tv = (TextView)pdLayout.findViewById(R.id.id_address_tv);
		mHeadCount_Tv = (TextView)pdLayout.findViewById(R.id.id_headcount_tv);
		mOwnerName_Tv = (TextView)pdLayout.findViewById(R.id.id_planowername);
		mOwnerPhoto_Iv = (ImageView) pdLayout.findViewById(R.id.imageview_owner);
		mfollowerPhotos_LL = (LinearLayout)pdLayout.findViewById(R.id.id_followersphoto_ll);
		mfollowerNames_LL = (LinearLayout)pdLayout.findViewById(R.id.id_followernames_ll);
		
		mfollowerPhotoLayouts = new LinearLayout[5];
		mfollowerPhotoLayouts[0] = (LinearLayout)pdLayout.findViewById(R.id.id_followerphoto1);
		mfollowerPhotoLayouts[1] = (LinearLayout)pdLayout.findViewById(R.id.id_followerphoto2);
		mfollowerPhotoLayouts[2] = (LinearLayout)pdLayout.findViewById(R.id.id_followerphoto3);
		mfollowerPhotoLayouts[3] = (LinearLayout)pdLayout.findViewById(R.id.id_followerphoto4);
		mfollowerPhotoLayouts[4] = (LinearLayout)pdLayout.findViewById(R.id.id_followerphoto5);
		mfollowerNameLayouts = new LinearLayout[5];
		mfollowerNameLayouts[0] = (LinearLayout)pdLayout.findViewById(R.id.id_followername1);
		mfollowerNameLayouts[1] = (LinearLayout)pdLayout.findViewById(R.id.id_followername2);
		mfollowerNameLayouts[2] = (LinearLayout)pdLayout.findViewById(R.id.id_followername3);
		mfollowerNameLayouts[3] = (LinearLayout)pdLayout.findViewById(R.id.id_followername4);
		mfollowerNameLayouts[4] = (LinearLayout)pdLayout.findViewById(R.id.id_followername5);
		mapView = (MapView) pdLayout.findViewById(R.id.map_View);
		baiduMap = mapView.getMap();
		baiduMap.setMyLocationEnabled(true);
		updateViews();
		
		return pdLayout;
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden)
		{
			updateViews();	
		}
	}

	private void updateViews() {
		mSportPlan = ((MainActivity)this.getActivity()).mSelectedPlan;
		mSportType_Tv.setText(mSportPlan.getSportType().getName());
		mPlanTime_Tv.setText(DateUtil.toHourMinute(mSportPlan.getStartDate()) + "-" +
				DateUtil.toHourMinute(mSportPlan.getEndDate()));
		mHeadCount_Tv.setText(mSportPlan.getFollowers().length + "人");
		String memo = mSportPlan.getMemo();
		mMemo_Tv.setText((memo == "" || memo == null ) ? "无备注": memo);
		
		mOwnerName_Tv.setText(mSportPlan.getOwner().getName());
		
		User[] followers = mSportPlan.getFollowers();			
		for (int i = 0; i < followers.length; i++)
		{
			ImageView photo = new ImageView((MainActivity)this.getActivity());
			photo.setImageResource(R.drawable.headportrait);
			LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, 
					LayoutParams.MATCH_PARENT);

			photo.setLayoutParams(lp);

			if (i < 5)
			{
				mfollowerPhotoLayouts[i].addView(photo);
			}
			
			TextView name = new TextView((MainActivity)this.getActivity());
			name.setText(followers[i].getName());
			name.setTextSize(11);
			name.setLayoutParams(lp);
			name.setGravity(Gravity.CENTER);
			if (i < 5)
			{
				mfollowerNameLayouts[i].addView(name);
			}
		}
		
		// 设置我的位置
		Location myLocation = ((MainActivity)this.getActivity()).mLocation;
		MyLocationData.Builder locationBuilder = new MyLocationData.Builder();
		locationBuilder.latitude(myLocation.getLatitude());
		locationBuilder.longitude(myLocation.getLongitude());
		MyLocationData locationData = locationBuilder.build();
		baiduMap.setMyLocationData(locationData);
		
		Location location = mSportPlan.getLocation();
		if	(location.getLatitude() == 0 && location.getAltitude() == 0)
		{
			mapView.setVisibility(View.GONE);
			mAddress_Tv.setText("未指定地址");
		}
		else
		{
			mapView.setVisibility(View.VISIBLE);
			mAddress_Tv.setText(mSportPlan.getAddress());
		}
		navigateTo(mSportPlan.getLocation());
	}
		
	private void navigateTo(Location location) {

		LatLng point = new LatLng(location.getLatitude(),
				location.getLongitude());
		MapStatusUpdate update = MapStatusUpdateFactory.newLatLng(point);
		baiduMap.animateMapStatus(update);
		update = MapStatusUpdateFactory.zoomTo(16f);
		baiduMap.animateMapStatus(update);
		
		//构建Marker图标  
		BitmapDescriptor bitmap = BitmapDescriptorFactory  
		    .fromResource(R.drawable.icon_gcoding);  
		//构建MarkerOption，用于在地图上添加Marker  
		OverlayOptions option = new MarkerOptions()  
		    .position(point)  
		    .icon(bitmap);  
		//在地图上添加Marker，并显示  
		baiduMap.addOverlay(option);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		baiduMap.setMyLocationEnabled(false);
		mapView.onDestroy();
	}

	@Override
	public void onPause() {
		super.onPause();
		mapView.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		mapView.onResume();
	}
}
