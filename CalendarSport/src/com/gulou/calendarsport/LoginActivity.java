package com.gulou.calendarsport;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.gulou.calendarsport.util.GlobalData;
import com.gulou.calendarsport.util.HttpUtil;
import com.gulou.calendarsport.util.HttpUtil.HttpCallbackListener;
import com.tencent.connect.UserInfo;
import com.tencent.connect.auth.QQAuth;
import com.tencent.connect.auth.QQToken;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class LoginActivity extends Activity implements OnClickListener {

	private Button loginButton;
	private Tencent mTencent;
	public static QQAuth mQQAuth;
	public static String mAppid;
	public static String openidString;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		loginButton = (Button) findViewById(R.id.buttonLogin);
		loginButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.buttonLogin:
			LoginQQ();
			break;

		default:
			break;
		}
	}

	private void LoginQQ() {
		mAppid = "222222";//"1104070887";
		mTencent = Tencent.createInstance(mAppid, getApplicationContext());
		mTencent.login(this, "all", new BaseUiListener());
		// mTencent.
	}

	private class BaseUiListener implements IUiListener {

		public void onCancel() {

		}

		public void onComplete(Object response) {
			Toast.makeText(getApplicationContext(), "登录成功", Toast.LENGTH_SHORT).show();
			try {
				openidString = ((JSONObject) response).getString("openid");
				final String access_token = ((JSONObject) response).getString("access_token");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			QQToken qqToken = mTencent.getQQToken();
			UserInfo info = new UserInfo(getApplicationContext(), qqToken);
			
			info.getUserInfo(new IUiListener() 
			{
				public void onComplete(final Object response) 
				{
					JSONObject node = (JSONObject) response;
					String nickName = null;
					try {
						nickName = node.getString("nickname");
					} catch (JSONException e) {
						// TODO 自动生成的 catch 块
						e.printStackTrace();
					}
					Intent intent = new Intent();
					intent.setClass(LoginActivity.this, EditProfileActivity.class);
					intent.putExtra("openidString", openidString);
					intent.putExtra("qqName", nickName);
					
					startActivityForResult(intent, 100);
					
				}

				public void onCancel() {

				}

				public void onError(UiError arg0) 
				{
					Toast.makeText(getApplicationContext(), "error", 0).show();
				}

			});

		}

		@Override
		public void onError(UiError arg0) {
			Toast.makeText(getApplicationContext(), "error", 0).show();
		}
	}
	
	@Override 
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        if (requestCode == 100){
        	finish();
        }  
    }  

}
