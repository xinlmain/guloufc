package com.gulou.calendarsport.bll;

import java.io.Serializable;

public class SportType implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8859539198958197240L;
	private int id;
	private String name;
	
	public SportType()
	{}
	
	public SportType(int i, String string) {
		this.id = i;
		this.name = string;
	}
	public int getId() {
		return id;
	}
	public void setId(int sportId) {
		this.id = sportId;
	}
	public String getName() {
		return name;
	}
	public void setName(String sportName) {
		this.name = sportName;
	}

}
