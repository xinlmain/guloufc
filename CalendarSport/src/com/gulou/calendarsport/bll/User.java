package com.gulou.calendarsport.bll;

import java.io.Serializable;

public class User implements Serializable
{

	public User() {
		
	}
	private int id;
	private String name;
	public User(int i, String string) {
		this.id = i;
		this.name = string;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
