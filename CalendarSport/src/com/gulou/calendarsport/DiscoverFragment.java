package com.gulou.calendarsport;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gulou.calendarlib.widget.CalendarView;
import com.gulou.calendarlib.util.CustomDate;
import com.gulou.calendarlib.widget.CalendarView.onClickDateListener;
import com.gulou.calendarlib.widget.SlideCalendar;
import com.gulou.calendarlib.widget.CalendarView.onCalendarChangeListener;
import com.gulou.calendarlib.widget.SlideCalendar.onChosenDateChangeListener;
import com.gulou.calendarlib.widget.SlideCalendar.onShowDateChangeListener;
import com.gulou.calendarsport.bll.SportPlan;
import com.gulou.calendarsport.bll.SportType;
import com.gulou.calendarsport.bll.User;
import com.gulou.calendarsport.util.HttpUtil;
import com.gulou.calendarsport.util.HttpUtil.HttpCallbackListener;

import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class DiscoverFragment extends Fragment {
	private static final String TAG = "DiscoverFragment";
	private FragmentActivity mainActivity;
	private ListView mPlanListView;
	private ArrayList<SportPlan> mPlanList;
	private SlideCalendar mSlideCalendar;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainActivity = this.getActivity();
		mPlanList = this.getTestSportPlanList();
		this.getNearbyPlanList();
		
		View discoverView = inflater.inflate(R.layout.fragment_discover,
				container, false);
		mSlideCalendar = (SlideCalendar) discoverView.findViewById(R.id.id_slide_calendar);
		CustomDate showDate = mSlideCalendar.getShowDate();
		((MainActivity)mainActivity).setShowDateViewText(showDate.year, showDate.month);
		mSlideCalendar.SetOnShowDateChangeListener(new onShowDateChangeListener() {
			
			@Override
			public void onShowDateChange(CustomDate date) {
				((MainActivity)mainActivity).setShowDateViewText(date.year, date.month);
			}
		});
		mSlideCalendar.setOnChosenDateChangeListener(new onChosenDateChangeListener() {
			
			@Override
			public void onChosenDateChange(CustomDate date) {
				Toast.makeText(mainActivity, date.toString(), Toast.LENGTH_SHORT).show();
			}
		});
		
		((MainActivity)mainActivity).setSlideCalendarForMainActivity(mSlideCalendar);
		
		mPlanListView = (ListView)discoverView.findViewById(R.id.id_plan_listView);
		
		PlanListAdapter pa = new PlanListAdapter(mainActivity, mPlanList);
		mPlanListView.setAdapter(pa);
		mPlanListView.setCacheColorHint(0);
		mPlanListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				((MainActivity)mainActivity).ShowPlanDetailFragment(mPlanList.get(arg2));
			}
			
		});
		
		//getFeeds();
		return discoverView;
	}

	private ArrayList<SportPlan> getNearbyPlanList() {
		Location location = ((MainActivity)getActivity()).mLocation;
		JSONObject node = new JSONObject();
		try {
			node.put("x", location.getLongitude());
			node.put("y", location.getLatitude());

		} catch (JSONException e1) {
			// TODO �Զ����ɵ� catch ��
			e1.printStackTrace();
		}
		Log.d(TAG, node.toString());

		HttpUtil.sendPostHttpRequest("http://120.24.85.99:8080/lbs", node.toString(), new HttpCallbackListener() {
			
			@Override
			public void onFinish(String response) {
				Log.d(TAG, response);
				JSONArray arr;
				try {
					arr = new JSONArray(response);
					ArrayList<SportPlan> plans = new ArrayList<SportPlan>();
					for (int i = 0; i < arr.length(); i++) {
						JSONObject temp = (JSONObject) arr.get(i);
						SportPlan sp = new SportPlan();
						sp.setOwner(new User(temp.getInt("uid"), temp.getString("name")));
						sp.setMemo(temp.getString("memo"));
						

						plans.add(sp);
						//mPlanList = plans;
					}
				} catch (JSONException e) {
					// TODO �Զ����ɵ� catch ��
					e.printStackTrace();
				}
			}
			
			@Override
			public void onError(Exception e) {
				Log.d(TAG, e.toString());
			}
		});
		return null;
	}

	private ArrayList<SportPlan> getTestSportPlanList(){
		
		ArrayList<SportPlan> planList = new ArrayList<SportPlan>();
		
		SportPlan plan = new SportPlan();
		plan.setAddress("�ϴ��¥У���ٳ�");
		plan.setOwner(new User(1, "��κ"));
		plan.setDistance(2.8);
		plan.setSportType(new SportType(1, "����"));
		plan.setStartDate(new Date());
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MINUTE, 120);
		plan.setEndDate(c.getTime());
		plan.setMemo("�ǺǺ�");
		plan.setFollowers(new User[]{new User(3, "����"), new User(4, "��߮��"), new User(5, "��ΰ")});
		Location ol = ((MainActivity)mainActivity).mLocation;
		ol.setLatitude(ol.getLatitude() + 0.02);
		ol.setLongitude(ol.getLongitude() + 0.01);
		plan.setLocation(ol);
		planList.add(plan);
		
		plan = new SportPlan();
		plan.setAddress("��̨ɽ������");
		plan.setOwner(new User(2, "������"));
		plan.setDistance(10.0);
		plan.setSportType(new SportType(2,"��Ӿ"));
		plan.setStartDate(new Date());
		Calendar c2 = Calendar.getInstance();
		c2.add(Calendar.MINUTE, 90);
		plan.setEndDate(c2.getTime());
		plan.setMemo("������");
		plan.setFollowers(new User[]{new User(6, "����"), new User(7, "���ص�")});
		plan.setLocation(((MainActivity)mainActivity).mLocation);
		planList.add(plan);
		
		plan = new SportPlan();
		plan.setAddress("��̨ɽ������");
		plan.setOwner(new User(2, "��ĳ��"));
		plan.setDistance(10.0);
		plan.setSportType(new SportType(2,"����"));
		plan.setStartDate(new Date());
		plan.setEndDate(c2.getTime());
		plan.setMemo("");
		plan.setFollowers(new User[]{new User(6, "����"), new User(7, "���ص�")});
		plan.setLocation(((MainActivity)mainActivity).mLocation);
		planList.add(plan);
		
		plan = new SportPlan();
		plan.setAddress("�ϴ��¥У���ٳ�");
		plan.setOwner(new User(1, "�����"));
		plan.setDistance(2.8);
		plan.setSportType(new SportType(1, "���"));
		plan.setStartDate(new Date());
		plan.setEndDate(c.getTime());
		plan.setMemo("�ǺǺ�");
		plan.setFollowers(new User[]{new User(3, "����"), new User(4, "��߮��"), new User(5, "��ΰ")});
		plan.setLocation(((MainActivity)mainActivity).mLocation);
		planList.add(plan);
		
		plan = new SportPlan();
		plan.setAddress("����ѧԺ");
		plan.setOwner(new User(1, "����"));
		plan.setDistance(2.8);
		plan.setSportType(new SportType(1, "����"));
		plan.setStartDate(new Date());
		plan.setEndDate(c.getTime());
		plan.setMemo("�ǺǺ�");
		plan.setFollowers(new User[]{new User(3, "����"), new User(4, "��߮��"), new User(5, "��ΰ")});
		plan.setLocation(((MainActivity)mainActivity).mLocation);
		planList.add(plan);

		plan = new SportPlan();
		plan.setAddress("�Ӻ���ѧ");
		plan.setOwner(new User(1, "������"));
		plan.setDistance(2.8);
		plan.setSportType(new SportType(1, "ƹ����"));
		plan.setStartDate(new Date());
		plan.setEndDate(c.getTime());
		plan.setMemo("�ǺǺ�");
		plan.setFollowers(new User[]{new User(3, "����"), new User(4, "��߮��"), new User(5, "��ΰ")});
		Location location = new Location(LocationManager.NETWORK_PROVIDER);
		location.setLatitude(0);
		location.setLongitude(0);
		plan.setLocation(location);
		planList.add(plan);
		
		return planList;
	}

}
